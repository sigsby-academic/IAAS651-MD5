
OUT := out
NIX := $(OUT)/nix
WIN := $(OUT)/win

MD5_NIX := $(NIX)/md5
MD5_WIN := $(WIN)/md5.exe
MD5_SRC := src/main.cpp src/md5.cpp

FIND_NIX := $(NIX)/find
FIND_WIN := $(WIN)/find.exe
FIND_SRC := src/find.cpp src/md5.cpp

.PHONY: default md5_nix md5_win md5_all test find watch clean

default: md5_nix

md5_nix: $(MD5_NIX)

md5_win: $(MD5_WIN)

md5_all: $(MD5_NIX) $(MD5_WIN)

test: $(MD5_NIX)
	./test

find: $(FIND_NIX)
	$(FIND_NIX) | tee find.out

watch:
	watch 'sort find.out | uniq -dc'

clean:
	rm -rf $(OUT) *.out

# actual files

$(MD5_NIX): $(MD5_SRC)
	mkdir -p $(@D)
	g++ -o $(@) $(^)
	chmod a+x $(NIX)

$(MD5_WIN): $(MD5_SRC)
	mkdir -p $(@D)
	cp /usr/x86_64-w64-mingw32/bin/*.dll $(@D)/
	x86_64-w64-mingw32-g++ -o $(@) $(^)

$(FIND_NIX): $(FIND_SRC)
	mkdir -p $(@D)
	g++ -o $(@) $(^)
	chmod a+x $(NIX)

$(FIND_WIN): $(FIND_SRC)
	mkdir -p $(@D)
	cp /usr/x86_64-w64-mingw32/bin/*.dll $(@D)/
	x86_64-w64-mingw32-g++ -o $(@) $(^)
