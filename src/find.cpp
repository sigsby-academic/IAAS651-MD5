#include <iostream>
#include <cmath>
#include "md5.h"

using std::cout;
using std::endl;

int main(int argc, char** argv)
{
	if(sizeof(char) != 1) {
		std::cerr << "ERROR: char must be 1 bytes";
	}
	if(sizeof(int) != 4) {
		std::cerr << "ERROR: int must be 4 bytes";
	}
	
	unsigned long max = pow(2,9);
	unsigned long val = 0;
	char str[sizeof val];
	for(val; val<max; val++) {
		memcpy(str, &val, sizeof val);
		MD5 md5(str);
		cout << md5.hex(3) << endl;
	}
	return 0;
}

