#include <iostream>
#include "md5.h"

using std::cout;
using std::endl;

int main(int argc, char** argv)
{
	if(sizeof(char) != 1) {
		std::cerr << "ERROR: char must be 1 bytes";
	}
	if(sizeof(int) != 4) {
		std::cerr << "ERROR: int must be 4 bytes";
	}

	if(argc != 2) {
		std::cerr << "ERROR: missing input\n";
		return 1;
	}
	
	cout << md5(argv[1]) << endl;

	return 0;
}

