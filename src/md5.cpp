/*

R. David Sigsby
Davenport Univeristy
Winter Session 2, 2016
IAAS 651 Project

Credit to original creator:
   Network Working Group
   Request for Comments: 1321
   R. Rivest
   MIT Laboratory for Computer Scienceand RSA Data Security, Inc.
   April 1992
   http://tools.ietf.org/html/rfc1321
   
Inspiration (and some code) also borrowed from:
   https://github.com/B-Con/crypto-algorithms
   http://www.zedwood.com/article/cpp-md5-function
   
*/

#include "md5.h"
#include <cstdio>


// convenience function
std::string md5(const std::string str) {
    MD5 md5 = MD5(str);
    return md5.hex();
}

// iostream integration
std::ostream& operator<<(std::ostream& out, MD5 md5) {
   return out << md5.hex();
}


//============================================================================
// initialize class constants
//============================================================================

// initial register values
const MD5::word MD5::INIT[REG] = {
   0x67452301, 0xefcdab89, 0x98badcfe, 0x10325476
};
      
// for message padding
const MD5::byte MD5::P[BS] = {
   0x80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

// index for process algorithm
const unsigned int MD5::X[RND*STP] = {
   0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15,
   1,  6, 11,  0,  5, 10, 15,  4,  9, 14,  3,  8, 13,  2,  7, 12,
   5,  8, 11, 14,  1,  4,  7, 10, 13,  0,  3,  6,  9, 12, 15,  2,
   0,  7, 14,  5, 12,  3, 10,  1,  8,  15, 6,  13, 4, 11,  2,  9
};

// shift for process algorithm
const unsigned int MD5::S[RND*STP] = {
   7, 12, 17, 22,  7, 12, 17, 22,  7, 12, 17, 22,  7, 12, 17, 22,
   5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20,
   4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23,
   6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21
};

// constants for process algorithm
// derived from binary integer part of the sines of integers (Radians):
//    
//    for i from 0 to 63 (BS)
//       CONSTANT[i] := floor(232 × abs(sin(i + 1)))
//    end for
//
const unsigned int MD5::K[RND*STP] = {
   0xd76aa478, 0xe8c7b756, 0x242070db, 0xc1bdceee,
   0xf57c0faf, 0x4787c62a, 0xa8304613, 0xfd469501,
   0x698098d8, 0x8b44f7af, 0xffff5bb1, 0x895cd7be,
   0x6b901122, 0xfd987193, 0xa679438e, 0x49b40821,
   
   0xf61e2562, 0xc040b340, 0x265e5a51, 0xe9b6c7aa,
   0xd62f105d, 0x02441453, 0xd8a1e681, 0xe7d3fbc8,
   0x21e1cde6, 0xc33707d6, 0xf4d50d87, 0x455a14ed,
   0xa9e3e905, 0xfcefa3f8, 0x676f02d9, 0x8d2a4c8a,
   
   0xfffa3942, 0x8771f681, 0x6d9d6122, 0xfde5380c,
   0xa4beea44, 0x4bdecfa9, 0xf6bb4b60, 0xbebfbc70,
   0x289b7ec6, 0xeaa127fa, 0xd4ef3085, 0x04881d05,
   0xd9d4d039, 0xe6db99e5, 0x1fa27cf8, 0xc4ac5665,
   
   0xf4292244, 0x432aff97, 0xab9423a7, 0xfc93a039,
   0x655b59c3, 0x8f0ccc92, 0xffeff47d, 0x85845dd1,
   0x6fa87e4f, 0xfe2ce6e0, 0xa3014314, 0x4e0811a1,
   0xf7537e82, 0xbd3af235, 0x2ad7d2bb, 0xeb86d391
};

//============================================================================
// constructors
//============================================================================

MD5::MD5() {
   init();
}

MD5::MD5(const std::string &text) {
   init();
   update(text.c_str(), text.length());
   finalize();
}

//============================================================================
// user interface functions
//============================================================================

void MD5::init() {
   finalized=false;
   for(int i=0; i<LEN; i++) bitlength[i] = 0;
   for(int i=0; i<REG; i++) registers[i] = INIT[i];
}

// initiates or continues a processing operation
void MD5::update(const char input[], size length) {
   update((const unsigned char*)input, length);
}
void MD5::update(const unsigned char input[], size length) {
   // compute number of bytes mod 64
   size index = bitlength[0] / 8 % BS;
   
   // update bitlength of original message
   if((bitlength[0] += (length << 3)) < (length << 3)) {
      bitlength[1]++;
   }
   bitlength[1] += (length >> 29);
   
   // number of bytes we need to fill in final
   size firstpart = 64 - index;

   size i;

   // process 
   if (length >= firstpart) {
      // fill final first, process
      memcpy(&final[index], input, firstpart);
      process(final);
      
      // process chunks of BS (64 bytes)
      for (i = firstpart; i + BS <= length; i += BS) {
         process(&input[i]);
      }
      index = 0;
   } else {
      i = 0;
   }
   // final remaining input
   memcpy(&final[index], &input[i], length-i);
}

// padding, append length, and process final block(s)
// returns reference to self for chaining/istream integration
MD5& MD5::finalize() {
   if (!finalized) {
      // number of bits
      unsigned char bits[8];
      encode(bits, bitlength, 8);
      
      // add padding
      size index = bitlength[0] / 8 % 64;
      size padLen = (index < 56) ? (56 - index) : (120 - index);
      update(P, padLen);
      
      // append original message length
      update(bits, 8);
      
      // store registers in result
      encode(result, registers, RES);
      
      // clean sensitive information
      memset(final, 0, sizeof final);
      memset(bitlength, 0, sizeof bitlength);
      
      finalized=true;
   }
   return *this;
}

//============================================================================
// helper functions
//============================================================================

// decodes input (byte) into output (word)
// assumes len is a multiple of 4
void MD5::decode(word output[], const byte input[], size len) {
   for (unsigned int i = 0, j = 0; j < len; i++, j += 4) {
      output[i] = ((word)input[j]) 
         | (((word)input[j+1]) << 8) 
         | (((word)input[j+2]) << 16)
         | (((word)input[j+3]) << 24)
      ;
   }
}

// encodes input (word) into output (byte)
// assumes len is a multiple of 4
void MD5::encode(byte output[], const word input[], size len) {
   for (size i = 0, j = 0; j < len; i++, j += 4) {
      output[j] = input[i] & 0xff;
      output[j+1] = (input[i] >> 8) & 0xff;
      output[j+2] = (input[i] >> 16) & 0xff;
      output[j+3] = (input[i] >> 24) & 0xff;
   }
}

// return hex representation of result as string
std::string MD5::hex(int size /*=16*/) {
   if (!finalized) {
      return "";
   }
   // hex output, 1 larger than hex string for null terminator
   char out[size*2+1];
   // convert each char to hex
   for (int i=0; i<size; i++) {
      sprintf(out+i*2, "%02x", result[i]);
   }
   // add null terminator for string
   out[size*2]=0;
   // return output as string
   return std::string(out);
}

//============================================================================
// processing
//============================================================================
 
void MD5::process(const byte block[BS]) {
   word a = registers[0], b = registers[1], c = registers[2], d = registers[3], x[16];
   decode (x, block, BS);
   
   int i=0;
   for(int j=0; j<4; j++) {
      for(int k=0; k<4; k++, i++) {
         FF (a, b, c, d, x[X[i]], S[i], K[i]);
         word t = d; d = c; c = b; b = a; a = t;
      }
   }
   for(int j=0; j<4; j++) {
      for(int k=0; k<4; k++, i++) {
         GG (a, b, c, d, x[X[i]], S[i], K[i]);
         word t = d; d = c; c = b; b = a; a = t;
      }
   }
   for(int j=0; j<4; j++) {
      for(int k=0; k<4; k++, i++) {
         HH (a, b, c, d, x[X[i]], S[i], K[i]);
         word t = d; d = c; c = b; b = a; a = t;
      }
   }
   for(int j=0; j<4; j++) {
      for(int k=0; k<4; k++, i++) {
         II (a, b, c, d, x[X[i]], S[i], K[i]);
         word t = d; d = c; c = b; b = a; a = t;
      }
   }
   
   registers[0] += a;
   registers[1] += b;
   registers[2] += c;
   registers[3] += d;
   
   // clean sensitive information.
   memset(x, 0, sizeof x);
}

MD5::word MD5::F(word x, word y, word z) {
   return x&y | ~x&z;
}

MD5::word MD5::G(word x, word y, word z) {
   return x&z | y&~z;
}

MD5::word MD5::H(word x, word y, word z) {
   return x^y^z;
}

MD5::word MD5::I(word x, word y, word z) {
   return y ^ (x | ~z);
}

MD5::word MD5::rotate_left(word x, int n) {
   return (x << n) | (x >> (32-n));
}

void MD5::FF(word &a, word b, word c, word d, word x, word s, word ac) {
   a = rotate_left(a+ F(b,c,d) + x + ac, s) + b;
}

void MD5::GG(word &a, word b, word c, word d, word x, word s, word ac) {
   a = rotate_left(a + G(b,c,d) + x + ac, s) + b;
}

void MD5::HH(word &a, word b, word c, word d, word x, word s, word ac) {
   a = rotate_left(a + H(b,c,d) + x + ac, s) + b;
}

void MD5::II(word &a, word b, word c, word d, word x, word s, word ac) {
   a = rotate_left(a + I(b,c,d) + x + ac, s) + b;
}
