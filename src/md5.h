/*

R. David Sigsby
Davenport Univeristy
Winter Session 2, 2016
IAAS 651 Project

Credit to original creator:
   Network Working Group
   Request for Comments: 1321
   R. Rivest
   MIT Laboratory for Computer Scienceand RSA Data Security, Inc.
   April 1992
   http://tools.ietf.org/html/rfc1321
   
Inspiration (and some code) also borrowed from:
   https://github.com/B-Con/crypto-algorithms
   http://www.zedwood.com/article/cpp-md5-function
   
*/

#ifndef MD5_H
#define MD5_H

#include <cstring>
#include <iostream>


// convenience function
std::string md5(const std::string str);

class MD5 {
   
   public:
      
      typedef unsigned int  size; // type for sizes
      typedef unsigned char byte;
      typedef unsigned int  word;
      
      // constants & counters
      static const unsigned int BS  = 64; // blocksize
      static const unsigned int LEN =  2; // storage size of mesage bit length
      static const unsigned int REG =  4; // # of registers
      static const unsigned int RND =  4; // # of rounds
      static const unsigned int STP = 16; // steps per round
      static const unsigned int RES = 16; // size of result
      //static const unsigned char OUT = 3; // size of output, in bytes
      

      // initial register values
      static const MD5::word INIT[REG];
      
      // for message padding
      static const MD5::byte P[BS];
      
      // inputs for process algorithm (index, shift, constant)
      static const unsigned int X[RND*STP];
      static const unsigned int S[RND*STP];
      static const unsigned int K[RND*STP];
      
      // constructors
      MD5();
      MD5(const std::string& text);
      
      // initialize registers, etc.
      void init();
      
      // start or continue processing
      void update(const char *buf, size length);
      void update(const unsigned char *buf, size length);
      
      // finalize processing (padding, append length, final result)
      MD5& finalize();
      
      // get hex representation of result
      std::string hex(int size=16);
      
      // integrate with iostream
      friend std::ostream& operator<<(std::ostream&, MD5 md5);
      
   private:
      
      bool finalized;
      word bitlength[LEN]; // number of bits in original message (lo, hi)
      word registers[REG]; // processsing registers
      byte final[BS];      // left-over bytes after full blocks processed
      byte result[RES];    // the result
      
      // helper functions for dealling with endian-ness
      static void decode(word output[], const byte input[], size len);
      static void encode(byte output[], const word input[], size len);
      
      // process each block
      void process(const byte block[BS]);
      
      // processing functions
      word F(word x, word y, word z);
      word G(word x, word y, word z);
      word H(word x, word y, word z);
      word I(word x, word y, word z);
      word rotate_left(word x, int n);
      void FF(word &a, word b, word c, word d, word x, word s, word ac);
      void GG(word &a, word b, word c, word d, word x, word s, word ac);
      void HH(word &a, word b, word c, word d, word x, word s, word ac);
      void II(word &a, word b, word c, word d, word x, word s, word ac);
};

#endif